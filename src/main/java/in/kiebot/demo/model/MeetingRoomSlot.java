package in.kiebot.demo.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class MeetingRoomSlot {
    @Id
    @GeneratedValue
    private int id;
    private int meetingRoomId;

    private int slotId;





}
