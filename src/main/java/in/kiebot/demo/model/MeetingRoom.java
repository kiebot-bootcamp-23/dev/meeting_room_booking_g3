package in.kiebot.demo.model;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.bind.annotation.RequestMapping;

@Entity
@Getter
@Setter
@ToString
public class MeetingRoom {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private int capacity;
    private Boolean airConditioned;
    private Boolean isAvailable;

}
