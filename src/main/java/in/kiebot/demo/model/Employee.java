package in.kiebot.demo.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Employee {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String address;

    private int company_id;
//    @ManyToOne
//    @JoinColumn(name="companyId")
//    @JsonIgnore
//    private Company company;

    private String phoneno;


}
