package in.kiebot.demo.dto;

public interface MeetingRoomDTO {
    public String getMeetingRoom();
    public String getSlot();

}
