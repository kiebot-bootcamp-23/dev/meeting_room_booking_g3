package in.kiebot.demo.controler;

import in.kiebot.demo.model.MeetingRoom;
import in.kiebot.demo.repository.MeetingRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("api/v1/")
public class MeetingRoomController {
    @Autowired
    MeetingRoomRepository meetingRoomRepository;

    @PostMapping("meetingrooms")
    ResponseEntity createMeetingRoom(@RequestBody MeetingRoom meetingroom) {
        return ResponseEntity.ok(meetingRoomRepository.save(meetingroom));
    }


    @GetMapping(value = "meetingrooms/{id}")
    ResponseEntity getMeetingRoomWith(@PathVariable("id") Integer id) {
        Optional<MeetingRoom> optionalSkill = meetingRoomRepository.findById(id);
        if (optionalSkill.isPresent()) {
            return ResponseEntity.ok(optionalSkill.get());
        }
        return ResponseEntity.badRequest().body("The skill is not present");
    }

    @GetMapping("meetingrooms")
    ResponseEntity getMeetingRoom() {
        return ResponseEntity.ok(meetingRoomRepository.findAll());
    }

    @PutMapping("meetingrooms/{id}")
    ResponseEntity updateMeetingRoom(@PathVariable Integer id,@RequestBody MeetingRoom meetingRoom) {
        Optional<MeetingRoom>  optionalMeetingRoom = meetingRoomRepository.findById(id);
        if(optionalMeetingRoom.isEmpty()){
            return ResponseEntity.badRequest().body("id not exist");
        }
        MeetingRoom existingMeetingRoom = optionalMeetingRoom.get();
        existingMeetingRoom.setName(meetingRoom.getName());
        existingMeetingRoom.setCapacity(meetingRoom.getCapacity());
        existingMeetingRoom.setAirConditioned(meetingRoom.getAirConditioned());
        existingMeetingRoom.setIsAvailable(meetingRoom.getIsAvailable());
        return ResponseEntity.ok(meetingRoomRepository.save(existingMeetingRoom));
    }



    @DeleteMapping(value = "meetingrooms/{id}")
    ResponseEntity deleteMeetingRoom(@PathVariable Integer id) {
        meetingRoomRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}


