package in.kiebot.demo.controller;

import in.kiebot.demo.model.Employee;
import in.kiebot.demo.model.Slot;
import in.kiebot.demo.repository.EmployeeRepository;
import in.kiebot.demo.repository.SlotRepository;
import in.kiebot.demo.service.EmployeeService;
import in.kiebot.demo.service.SlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class SlotController {

    @Autowired
    SlotService slotService;

    @Autowired
    SlotRepository slotRepository;


    @GetMapping("/slots")
    public List<Slot> findAllSlot(){

        return slotService.findAllSlot();

    }
    @GetMapping("/slots/{id}")
    public List<Slot> findAllSlotById(@PathVariable("id") Integer id ){
        return slotService.findAllSlotById(id);
    }

    @PutMapping("/slots/{id}")
    ResponseEntity updateSlot(@PathVariable("id") Integer id, @RequestBody Slot slot) {

        Optional<Slot> optional = slotRepository.findById(id);
        if (optional.isEmpty()) {
            return ResponseEntity.badRequest().body("Not Found");
        }
        Slot existingSlot = optional.get();
        existingSlot.setSlot(slot.getSlot());

        return ResponseEntity.ok(slotRepository.save(existingSlot));

    }

    @DeleteMapping("/slots/{id}")
    ResponseEntity deleteSlot(@PathVariable("id") Integer id) {

        slotRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/slots")
    public Slot createSlot(@RequestBody Slot slot   ) {

        return slotRepository.save(slot);
    }

}
