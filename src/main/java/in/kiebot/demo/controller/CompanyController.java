 package in.kiebot.demo.controller;

 import in.kiebot.demo.model.Company;
 import in.kiebot.demo.repository.CompanyRepository;
 import in.kiebot.demo.service.CompanyService;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.http.ResponseEntity;
 import org.springframework.web.bind.annotation.*;

 import java.util.List;
 import java.util.Optional;

 @RestController
 @RequestMapping("/api/v1")
 public class CompanyController {

     @Autowired
     CompanyRepository companyRepository;

     @Autowired
     CompanyService companyService;

     @GetMapping("/companies")
     List<Company> getAllCompany(){
         return companyRepository.findAll();
     }

     @PostMapping("/companies")
     public ResponseEntity postCompany(@RequestBody Company company){
         return ResponseEntity.ok(companyRepository.save(company));
     }

     @GetMapping("/companies/{id}")
     public ResponseEntity getCompanyById(@PathVariable("id") Long id){
         final Optional<Company> optionalCompany = companyRepository.findById(id);
         if(optionalCompany.isEmpty()){
             return  ResponseEntity.badRequest().body("Could not find ");
         }
         return  ResponseEntity.ok(optionalCompany.get());
     }

     @DeleteMapping("/companies/{id}")
     public ResponseEntity deleteCompany(@PathVariable("id") Long id){
         companyRepository.deleteById(id);
         return ResponseEntity.ok("Success");
     }

     @PutMapping("/companies/{id}")
     public ResponseEntity updateCompany(@PathVariable("id") Long id, @RequestBody Company company){
             return companyService.updateAndSaveCompany(id,company);
     }
 }
