package in.kiebot.demo.controller;

import in.kiebot.demo.dto.MeetingRoomDTO;
import in.kiebot.demo.model.Employee;
import in.kiebot.demo.model.MeetingRoomSlot;
import in.kiebot.demo.model.Slot;
import in.kiebot.demo.repository.MeetingRoomSlotRepository;
import in.kiebot.demo.repository.SlotRepository;
import in.kiebot.demo.service.MeetingRoomSlotService;
import in.kiebot.demo.service.SlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class MeetingRoomSlotController {
    @Autowired
    MeetingRoomSlotService slotService;

    @Autowired
    MeetingRoomSlotRepository slotRepository;

    @GetMapping("/meetingroomslots")
    public List<MeetingRoomSlot> findAllMeetingRoomSlot(){

        return slotService.findAllMeetingRoomSlot();

    }

    @GetMapping("/meetingslots")
    public List<MeetingRoomDTO> findAllSlotsByMeetingRoom(){

        return slotService.getAllSlotsByMeetingRoom();

    }

    @GetMapping("/meetingroomslots/meetingrooms/{id}")
    public List<MeetingRoomSlot> findAllMeetingRoomSlot(@PathVariable("id") Integer id ){

        return slotService.getAllMeetingRoomSlotByMeetingRoomId(id);

    }

    @DeleteMapping("/meetingroomslots/meetingrooms/{id}")
    ResponseEntity deleteSlot(@PathVariable("id") Integer id) {

        slotRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/meetingroomslots")
    public MeetingRoomSlot createSlot(@RequestBody MeetingRoomSlot slot   ) {

        return slotRepository.save(slot);
    }


}
