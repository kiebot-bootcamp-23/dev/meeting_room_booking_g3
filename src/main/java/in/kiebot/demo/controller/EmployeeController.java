package in.kiebot.demo.controller;


import in.kiebot.demo.model.Employee;
import in.kiebot.demo.repository.EmployeeRepository;
import in.kiebot.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    EmployeeRepository employeeRepository;


    @GetMapping("/employees")
    public List<Employee> findAllEmployees(){

        return employeeService.findAllEmployees();

    }
    @GetMapping("/employees/{id}")
    public List<Employee> findAllEmployeeById(@PathVariable("id") Integer id ){
        return employeeService.findAllEmployeesById(id);
    }

    @PutMapping("/employees/{id}")
    ResponseEntity updateEmployees(@PathVariable("id") Integer id, @RequestBody Employee employee) {

        Optional<Employee> optionalskill = employeeRepository.findById(id);
        if (optionalskill.isEmpty()) {
            return ResponseEntity.badRequest().body("Not Found");
        }
        Employee existingEmployee = optionalskill.get();
        existingEmployee.setAddress(employee.getAddress());
        existingEmployee.setName(employee.getName());
        existingEmployee.setPhoneno(employee.getPhoneno());
        existingEmployee.setCompany_id(employee.getCompany_id());
        return ResponseEntity.ok(employeeRepository.save(existingEmployee));

    }

    @DeleteMapping("/employees/{id}")
    ResponseEntity deleteEmployee(@PathVariable("id") Integer id) {

        employeeRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/employees")
    public Employee createUser(@RequestBody Employee employee   ) {

        return employeeRepository.save(employee);

    }




}
