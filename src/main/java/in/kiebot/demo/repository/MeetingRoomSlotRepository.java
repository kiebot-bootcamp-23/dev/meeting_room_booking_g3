package in.kiebot.demo.repository;

import in.kiebot.demo.dto.MeetingRoomDTO;
import in.kiebot.demo.model.MeetingRoomSlot;
import in.kiebot.demo.model.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MeetingRoomSlotRepository extends JpaRepository<MeetingRoomSlot,Integer> {
    List<MeetingRoomSlot> findAllMeetingRoomSlotByMeetingRoomId(Integer id);

    @Query(value="select m.name as MeetingRoom ,p.slot as Slot  from meeting_room_slot s inner join meeting_room m on s.meeting_room_id=m.id inner join slot p on p.id=s.slot_id" ,nativeQuery = true)
    List<MeetingRoomDTO> findAllSlotsByMeetingRoom();

}
