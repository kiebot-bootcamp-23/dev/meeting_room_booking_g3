package in.kiebot.demo.service;


import in.kiebot.demo.dto.MeetingRoomDTO;
import in.kiebot.demo.model.MeetingRoomSlot;
import in.kiebot.demo.repository.MeetingRoomSlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MeetingRoomSlotService {
    @Autowired
    MeetingRoomSlotRepository slotRepository;

    public List<MeetingRoomSlot> findAllMeetingRoomSlot() {
        return slotRepository.findAll();
    }

    public List<MeetingRoomSlot> getAllMeetingRoomSlotByMeetingRoomId(Integer id) {
        return slotRepository.findAllMeetingRoomSlotByMeetingRoomId(id);
    }

    public List<MeetingRoomDTO> getAllSlotsByMeetingRoom() {
        return slotRepository.findAllSlotsByMeetingRoom();
    }

}
