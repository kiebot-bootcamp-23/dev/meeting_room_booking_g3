package in.kiebot.demo.service;

import in.kiebot.demo.model.Company;
import in.kiebot.demo.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;

@Service
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    public ResponseEntity updateAndSaveCompany(@PathVariable("id") Long id, @RequestBody Company company){
        Optional<Company> optionalCompany = companyRepository.findById(company.getId());
        if(optionalCompany.isEmpty()){
            return ResponseEntity.badRequest().body("could not find");
        }
        Company existingCompany=optionalCompany.get();
        existingCompany.setName(company.getName());
        existingCompany.setAddress(company.getAddress());
        existingCompany.setPhone(company.getPhone());
        return ResponseEntity.ok(companyRepository.save(existingCompany));
    }
}
