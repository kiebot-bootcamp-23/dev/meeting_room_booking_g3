package in.kiebot.demo.service;

import in.kiebot.demo.model.Employee;
import in.kiebot.demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;


    public List<Employee> findAllEmployees() {
        return employeeRepository.findAll();
    }

    public List<Employee> findAllEmployeesById(Integer id) {
        List<Employee> lstEmployee=employeeRepository.findAllEmployeeById(id);
        return lstEmployee;
    }


}
