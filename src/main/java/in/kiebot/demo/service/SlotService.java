package in.kiebot.demo.service;

import in.kiebot.demo.model.Employee;
import in.kiebot.demo.model.Slot;
import in.kiebot.demo.repository.EmployeeRepository;
import in.kiebot.demo.repository.SlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SlotService {
    @Autowired
    SlotRepository slotRepository;

    public List<Slot> findAllSlot() {
        return slotRepository.findAll();
    }

    public List<Slot> findAllSlotById(Integer id) {
        List<Slot> lstslot=slotRepository.findAllSlotById(id);
        return lstslot;
    }
}
